## Presequite

1. Node 12.x.x
2. Ant design 4.x.x
3. React 16.12.x
4. Redux 4.0.0
5. Eslint
6. Api https://www.coingecko.com/en/api#explore-api
7. Sample https://www.coingecko.com/en

---

## Overview

1. Use create-react-app to build codebase
2. Use ant design framework
3. Use react hooks and redux store
4. Layout: Header, Footer, Main
5. Table function: sorting, filtering, pagination (server side)
6. Have to: manage env, state of web-app 

---

## Naming convention

1. Filename: kebab-case
2. Component, Class: PascalCase
3. Variable: camelCase

Reference
```
https://github.com/airbnb/javascript/tree/master/react
```
---

## Directory tree
```
.
+--🗂public
|  +--📄favicon.ico
|  +--📄index.html
|  +--📄logo192.png
|  +--📄logo512.png
|  +--📄manifest.json
|  +--📄robots.txt
+--🗂src
|  +--🗂assets
|  +--🗂components
|     +--🗂ComponentA
|        +--📄ComponentA.jsx
|        +--📄ComponentA.scss
|  +--🗂constants
|  +--🗂services
|  +--🗂state
|     +--🗂actions
|     +--🗂reducers
|     +--🗂store
|  +--📄App.css
|  +--📄App.js
|  +--📄App.test.js
|  +--📄index.css
|  +--📄index.js
|  +--📄logo.svg
|  +--📄serviceWorker.js
|  +--📄setupTests.js
+--📄.gitignore
+--📄README.md
+--📄package.json
+--📄yarn.lock

```